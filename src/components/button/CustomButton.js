import React from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {Icon, icoMoonConfigSet} from '@components/icon/Icon';
import Colors from '@constants/Colors';
import ScaleDimen from '@constants/ScaleDimen';
import Spinner from 'react-native-loading-spinner-overlay';
import fontFamily from '@constants/fontFamily';

// use CustomButton
//  <CustomButton    borderColor={}  onPress={}  title={} />

export const CustomButton = props => {
  const {onPress, title, style, disabled, icon, titleStyle, loader} = props;
  return (
    <TouchableOpacity
      style={[
        styles.largeButtonContainer,
        style,
        disabled && {borderColor: Colors.LIGHT},
      ]}
      onPress={onPress}
      activeOpacity={0.5}
      disabled={disabled}>
      <Text
        style={[
          styles.largeButtonText,
          titleStyle,
          disabled && {color: Colors.LIGHT},
        ]}>
        {title}
      </Text>
      <Spinner color={Colors.PRIMARY_COLOR} visible={loader} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  largeButtonContainer: {
    backgroundColor: Colors.PRIMARY_COLOR,
    height: ScaleDimen.ms50,
    justifyContent: 'center',
    marginTop: '3%',
    borderRadius: ScaleDimen.ms20,
  },
  largeButtonText: {
    fontSize: ScaleDimen.ms18,
    fontWeight: 600,
    color: '#fff',
    textAlign: 'center',
    justifyContent: 'center',
    fontFamily: fontFamily.Roboto_Bold,
    paddingHorizontal: 5,
  },
});
