import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Color from '@constants/Colors';
import ScaleDimen from '@constants/ScaleDimen';
import Modal from "react-native-modal";
import fontFamily from '@constants/fontFamily';
import GradientIcon from '../icon/Gradienticon';
import Icons from '@constants/Icons';

export const CustomBottomHalfModal = (props) => {
    const { visibleModal,
        setvisibleModal,
        childComponent,
        title,
        style,
        containerStyle
    } = props
    const colors = [Color.RED, Color.PRIMARY_COLOR, Color.GREEN,]

    return (

        <Modal
            onBackButtonPress={() => setvisibleModal(false)}
            onBackdropPress={() => setvisibleModal(false)}
            isVisible={visibleModal}
            backdropOpacity={0.1}
            backdropTransitionOutTiming={0}
            style={styles.bottomModal}
        >
            <View style={styles.modalContainer}>
                <TouchableOpacity style={styles.closeIconcontainer} onPress={() => setvisibleModal(false)}>
                    <GradientIcon
                        name={Icons.cancel_circle}
                        size={ScaleDimen.ms20}
                        colors={colors}
                        style={{ color: Color.BLACK }}
                    />
                </TouchableOpacity>
                <View style={styles.modalContentContainer}>
                    <View style={styles.modalTitle}>
                        <Text style={styles.modalTitleTxt}>{title}</Text>
                    </View>
                    {childComponent}
                </View>
            </View>
        </Modal>
    )
}


const styles = StyleSheet.create({
    bottomModal: {
        justifyContent: "flex-end",
        margin: 0,
    },
    modalContainer: {
        backgroundColor: Color.LIGHT,
        minHeight: '20%'
    },
    closeIconcontainer: {
        padding: ScaleDimen.ms5,
        top: -ScaleDimen.ms30,
        backgroundColor: Color.LIGHT,
        zIndex: ScaleDimen.ms50,
        alignSelf: 'flex-end',
        height: ScaleDimen.ms30,
        width: ScaleDimen.ms30,
        borderRadius: ScaleDimen.ms15,
        alignItems: 'center',
        alignSelf: 'center'
    },
    modalContentContainer: {
        padding: ScaleDimen.ms15
    },
    modalTitle: {
        borderColor: Color.PRIMARY_COLOR,
        borderBottomWidth: ScaleDimen.ms3,
        paddingBottom: ScaleDimen.ms10,
    },
    modalTitleTxt: {
        color: Color.PRIMARY_COLOR,
        fontFamily: fontFamily.Roboto_Bold,
        fontSize: ScaleDimen.ms14,
        alignSelf: 'center'
    },
    closeText: {
        fontFamily: fontFamily.Roboto_Bold,
        fontSize: ScaleDimen.ms15
    }

})
