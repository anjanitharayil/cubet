import React from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import {Icon, icoMoonConfigSet} from '@components/icon/Icon';

import Icons from '@constants/Icons';
import {useNavigation} from '@react-navigation/native';
import Colors from '../../constants/Colors';
import ScaleDimen from '../../constants/ScaleDimen';

function Header(props) {
  const navigation = useNavigation();

  const handleMenuPress = () => {
    // Handle left icon press here
    // For example, navigate to a specific screen
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleMenuPress}>
        <Icon
          name={Icons.cheveronleft}
          style={{paddingRight: '5%'}}
          color={Colors.PRIMARY_COLOR}
          size={ScaleDimen.ms20}
          config={icoMoonConfigSet}
        />
      </TouchableOpacity>
      {/* Additional header content */}

      <Text style={styles.title}>{props.title}</Text>
      <View></View>
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  container: {
    // backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '5%',
  },
  title: {
    fontSize: 22,
    fontWeight: 500,
    color: Colors.PRIMARY_COLOR,
  },
});
