
import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    Text,
    Image,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    DrawerContentScrollView,
    DrawerItem,
} from '@react-navigation/drawer';

import { setSignOut, selectIsLoggedIn } from '@redux/slices/authSlice';
import { useDispatch, useSelector } from 'react-redux'
import { Icon, icoMoonConfigSet } from '../icon/Icon';
import ScaleDimen from '@constants/ScaleDimen';
import Icons from '@constants/Icons';
import Routes from '@navigation/Routes';
// import Images from '@config/Images';
import Colors from '@constants/Colors';
import fontFamily from '@constants/fontFamily';

export default function CustomDrawerContent(props) {
    const dispatch = useDispatch();
    // const userdata = useSelector((state) => state.userAuth);
    const _clearAll = async () => {
        try {
            await AsyncStorage.clear();
            console.log(' Storage Cleared');
        } catch (error) {
            console.log(error);
        }
    };

    const handleLogout = async () => {
        _clearAll()
        dispatch(setSignOut());
    }

    const Array2 = [
        {
            label: "Today's Delivery",
            icon: Icons.delivery_truck,
            stack: Routes.HomeTab,
            screen: Routes.Home,
        },
        {
            label: "Customers",
            icon: Icons.address_book,
            stack: Routes.HomeTab,
            screen: Routes.Customer,
        },
        {
            label: "Add Customer",
            icon: Icons.user_plus,
            stack: Routes.AddCustomer,
            screen: Routes.AddCustomer,
        },
        {
            label: "Privacy Policy",
            icon: Icons.file_text2,
            stack: Routes.AddCustomer,
            screen: Routes.AddCustomer,
        },
        {
            label: "Terms and Conditions",
            icon: Icons.paragraph_justify,
            stack: Routes.AddCustomer,
            screen: Routes.AddCustomer,
        },
        {
            label: "Logout",
            icon: Icons.circle_left,
            function: 'handleLogout',
        },
    ]


    return (
        <DrawerContentScrollView {...props}>
            <View style={{
                width: '100%', height: ScaleDimen.ms100
            }}>
                {/* <Image
                    style={{ width: '90%', height: '100%' }}
                    resizeMode={'contain'}
                    source={Images.logo_EN}
                /> */}
            </View>
            {/* <View style={{ alignSelf: 'center' }}>
                <View style={styles.row}>
                    <Text style={styles.titleTxt}>Name:</Text>
                    <Text style={styles.contentTxt}>{userdata.first_name} {userdata.last_name}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.titleTxt}>Email:</Text>
                    <Text style={styles.contentTxt}>{userdata.email}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.titleTxt}>Phone No.:</Text>
                    <Text style={styles.contentTxt}>{userdata.phn_no}</Text>
                </View>
            </View> */}
            <View style={[styles.drawerWrapper]}>
                {
                    (Array2).map((item, index) => {
                        if (!(Boolean(item.function))) {
                            return (
                                <DrawerItem
                                    icon={({ focused, color, size }) => (
                                        <Icon
                                            name={item.icon}
                                            size={ScaleDimen.ms20}
                                            config={icoMoonConfigSet}
                                            style={[styles.drawerIcon, { color: Colors.PRIMARY_COLOR }]}
                                        />
                                    )}
                                    label={({ focused, color }) =>
                                        <Text style={[styles.drawerLabel, { color: Colors.PRIMARY_COLOR }]}>{item.label}</Text>
                                    }
                                    onPress={() =>
                                        props.navigation.navigate(item.stack,
                                            item.screen && {
                                                screen: item.screen,
                                                params: {
                                                    statusBarIdenti: item.params ? item.params : ''
                                                }
                                            }
                                        )
                                    }

                                    style={[styles.drawerItem, {}]}
                                    key={index}
                                />
                            )
                        } else {
                            return (
                                <DrawerItem
                                    icon={({ focused, color, size }) => (
                                        <Icon
                                            name={item.icon}
                                            size={ScaleDimen.ms20}
                                            config={icoMoonConfigSet}
                                            style={[styles.drawerIcon, { color: Colors.PRIMARY_COLOR }]}
                                        />
                                    )}
                                    label={({ focused, color }) =>
                                        <Text style={[styles.drawerLabel, { color: Colors.PRIMARY_COLOR }]}>{item.label}</Text>
                                    }
                                    onPress={() =>
                                        item.function ? handleLogout() : null
                                    }

                                    style={[styles.drawerItem, {}]}
                                    // focused={activeItem == item.stack ? true : false}
                                    key={index}
                                />
                            )
                        }
                    })

                }
            </View>
        </DrawerContentScrollView>
    );
}



const styles = StyleSheet.create({
    drawerIcon: {
        color: Colors.PRIMARY_COLOR
    },
    drawerItem: {
        padding: ScaleDimen.ms5,
        borderBottomWidth: ScaleDimen.ms2,
        borderColor: Colors.LIGHT,
        backgroundColor: Colors.WHITE
    },
    drawerLabel: {
        fontFamily: fontFamily.Roboto_Bold,
        fontSize: ScaleDimen.ms14,
        color: Colors.PRIMARY_COLOR,
        left: ScaleDimen.ms5
    },
    drawerWrapper: {
        height: '50%'
        // backgroundColor: 'red'
    },
    row: {
        flexDirection: 'row',
    },
    // titleTxt: {
    //     fontFamily: fontFamily.Roboto_Bold,
    //     color: Colors.PRIMARY_COLOR,
    //     fontSize: ScaleDimen.ms14
    // },
    // contentTxt: {
    //     fontFamily: fontFamily.Roboto_Medium,
    //     color: Colors.GRAY,
    //     fontSize: ScaleDimen.ms14
    // }

});