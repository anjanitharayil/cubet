import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  logged: false,
};
const authSlice = createSlice({
  name: 'userAuth',
  initialState,
  reducers: {
    setSignIn: (state, action) => {
      state.logged = true;
    },
    setSignOut: state => {
      state.logged = false;
    },
  },
});

export const {setSignIn, setSignOut} = authSlice.actions;

export const selectIsLoggedIn = state => state.userAuth.logged;

export default authSlice.reducer;
