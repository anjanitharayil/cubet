import React from 'react';
import {StyleSheet, Text} from 'react-native';

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Routes from './Routes';

import Home from '@screens/Home';

import Colors from '@constants/Colors';
import ScaleDimen from '@constants/ScaleDimen';
import Icons from '@constants/Icons';
// import styles from '@styles/GlobalStyles';

import {Icon, icoMoonConfigSet} from '@components/icon/Icon';

const Tab = createMaterialBottomTabNavigator();

function HOMETABS({navigation}) {
  return (
    <Tab.Navigator
      initialRouteName={Routes.HomeTab}
      activeColor={Colors.PRIMARY_COLOR}
      inactiveColor={'#8C746A'}
      barStyle={{
        backgroundColor: Colors.WHITE,
      }}>
      <Tab.Screen
        name={Routes.Menu}
        component={Home}
        options={{
          tabBarLabel: null,
          tabBarIcon: ({color, focused}) => (
            <Icon
              name={Icons.home3}
              size={ScaleDimen.ms20}
              config={icoMoonConfigSet}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Routes.other2}
        component={Home}
        options={{
          tabBarLabel: null,
          tabBarIcon: ({color, focused}) => (
            <Icon
              name={Icons.location}
              size={ScaleDimen.ms20}
              config={icoMoonConfigSet}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Routes.other}
        component={Home}
        options={{
          tabBarBadge: 3,
          tabBarBadgeStyle: {top: '0', position: 'absolute'},
          tabBarLabel: null,
          tabBarIcon: ({color, focused}) => (
            <Icon
              name={Icons.cart}
              size={ScaleDimen.ms20}
              config={icoMoonConfigSet}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Routes.Profile}
        component={Home}
        options={{
          tabBarLabel: null,
          tabBarIcon: ({color, focused}) => (
            <Icon
              name={Icons.user}
              size={ScaleDimen.ms20}
              config={icoMoonConfigSet}
              color={color}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const AfterNavStack = createNativeStackNavigator();

const stackScreenOptions = {
  headerBackTitleVisible: false,
  // headerTintColor: colors.blackHeaderText,
  headerTitleAlign: 'center',
  cardStyle: {
    // backgroundColor: colors.white,
  },
};

const AfterLoginNav = ({navigation}) => {
  return (
    <AfterNavStack.Navigator
      screenOptions={stackScreenOptions}
      initialRouteName={Routes.HomeTab}>
      <AfterNavStack.Screen
        options={props => ({
          // header: () => <CustomHeader {...props} />,
          header: () => null,
        })}
        name={Routes.Menu}
        component={HOMETABS}
      />
    </AfterNavStack.Navigator>
  );
};

export default AfterLoginNav;

const styles = StyleSheet.create({});
