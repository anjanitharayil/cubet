import React, {useState, useEffect} from 'react';
import {BeforeLoginNav} from './BeforeNavStack';
import AfterNavStack from './AfterNavStack';
import {selectIsLoggedIn} from '@redux/slices/authSlice';
import {useDispatch, useSelector} from 'react-redux';
export default function App() {
  const isLoggedIn = useSelector(selectIsLoggedIn);

  if (!isLoggedIn) {
    return <BeforeLoginNav />;
  } else {
    return <AfterNavStack />;
  }
}
