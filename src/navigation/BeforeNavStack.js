import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Routes from './Routes';

import Login from '@screens/Login';



const BeforeNavStack = createNativeStackNavigator();

const stackScreenOptions = {
    headerBackTitleVisible: false,
    // headerTintColor: colors.blackHeaderText,
    headerTitleAlign: 'center',
    cardStyle: {
        // backgroundColor: colors.white,
    },
};

export const BeforeLoginNav = () => {
    return (
        <BeforeNavStack.Navigator
            screenOptions={stackScreenOptions}
            initialRouteName={Routes.Login}>
            <BeforeNavStack.Screen
                options={{ headerShown: false }}
                name={Routes.Login}
                component={Login}
            />

        </BeforeNavStack.Navigator>
    );
};
