import React, {useState, useEffect} from 'react';
import {LogBox, StatusBar} from 'react-native';
import Routes from './AppRoute';
import Splash from '@screens/Splash';
import Colors from '@constants/Colors';
import {useDispatch, useSelector} from 'react-redux';
import {setSignIn} from '@redux/slices/authSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  GestureHandlerRootView,
  NativeViewGestureHandler,
} from 'react-native-gesture-handler';

export default function Navigation({}) {
  const [invisible, setinvisible] = useState(true);

  const hideSplashScreen = () => {
    setinvisible(false);
  };
  const dispatch = useDispatch();

  useEffect(() => {
    // const getUserToken = async () => {

    //     const value = await AsyncStorage.getItem("UserToken");
    //     auth = JSON.parse(value)
    //     if (value) {
    //         const user = {
    //             isLoggedIn: auth.isLoggedIn,
    //             isFirstUser: auth.isFirstUser,
    //             email: auth.email,
    //             first_name: auth.first_name,
    //             last_name: auth.last_name,
    //             pin: auth.pin,
    //             token: auth.token,
    //             location: auth.location,
    //             pinentered: false,
    //             agent_id: '',
    //             phone: '',
    //             place: '',
    //             pincode: '',
    //             address: '',
    //             auth: '',
    //             email_verified_at: ''

    //         };

    //         dispatch(setSignIn(user));
    //     }
    // }
    // getUserToken();

    setTimeout(() => {
      hideSplashScreen();
    }, 4500);
  }, []);

  LogBox.ignoreAllLogs(true);
  if (invisible) {
    return (
      <>
        <StatusBar hidden />
        <Splash />
      </>
    );
  } else {
    return (
      <GestureHandlerRootView style={{flex: 1}}>
        {Platform.OS === 'android' && (
          <StatusBar
            barStyle="dark-content"
            backgroundColor={Colors.WHITE}
            animated={true}
          />
        )}
        <Routes />
      </GestureHandlerRootView>
    );
  }
}
