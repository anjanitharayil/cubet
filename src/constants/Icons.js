export default {
  cheveronleft: 'cheveron-left',
  cheveronright: 'cheveron-right',
  home3: 'home3',
  location: 'location',
  cart: 'cart',
  user: 'user',
};
