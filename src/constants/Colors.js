export default {
  BGCOLOR: '#fff',
  PRIMARY_COLOR: '#B98068',
  LIGHT: '#e7f0fe',
  MID: '#0e65f1',
  DARK: '#0a1557',

  ORANGE: '#ffa500',
  BLACK: '#1c1b1a',
  WHITE: '#FFFFFF',

  DARK_RED: '#780707',
  RED: '#f73f07',

  GRAY: '#979797',
  DARK_GRAY: '#3E4041',
  DIM_GRAY: '#5E5E5F',

  OFF_BLUE: '#e6eff2',
  GREEN: '#00B199',
  VIOLET: '#ae10de',
  LIGHT_BLUE: '#3455fa',
};
