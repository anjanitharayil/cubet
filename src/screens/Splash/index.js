import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  View,
} from 'react-native';
import Colors from '../../constants/Colors';

const YourApp = () => {
  const screenWidth = Dimensions.get('window').width;
  const screenHeight = Dimensions.get('window').height;
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/splash.png')}
        style={{width: screenWidth, height: screenHeight}}
        resizeMode="cover"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.BGCOLOR,
    flex: 1,
  },
});

export default YourApp;
