import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Colors from '../../constants/Colors';
import Header from '../../components/header/Header1';
import ScaleDimen from '../../constants/ScaleDimen';
import {Icon, icoMoonConfigSet} from '@components/icon/Icon';

import Icons from '@constants/Icons';

function MyComponent({item}) {
  return (
    <TouchableOpacity style={styles.row}>
      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{uri: item.strDrinkThumb}} />
        <Text style={styles.title}>{item.strDrink}</Text>
      </View>
      <Icon
        name={Icons.cheveronright}
        style={{paddingRight: '5%'}}
        color={Colors.PRIMARY_COLOR}
        size={ScaleDimen.ms20}
        config={icoMoonConfigSet}
      />
    </TouchableOpacity>
  );
}

function Home() {
  // const navigation = useNavigation();

  const [items, setItems] = useState([]);
  const [Loader, setLoader] = useState(true);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch(
        'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=coffee',
      ); // Replace with your API endpoint
      const data = await response.json();
      setItems(data['drinks']);
      setLoader(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.main}>
      <Header title="Menu" />

      <ScrollView contentContainerStyle={styles.container}>
        {!Loader ? (
          <View>
            {items.map((item, id) => (
              <MyComponent item={item} key={id} />
            ))}
          </View>
        ) : (
          <Text style={{alignSelf: 'center'}}>Loading.....</Text>
        )}
      </ScrollView>
    </View>
  );
}

export default Home;

const styles = StyleSheet.create({
  main: {height: '100%', backgroundColor: Colors.WHITE},

  container: {
    flexGrow: 1,
    paddingTop: '5%',
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5%',
    borderBottomWidth: 0.5,
    borderColor: '#C69A80',
    alignItems: 'center',
    paddingVertical: 8,
  },
  title: {
    fontWeight: '500',
    fontSize: ScaleDimen.ms20,
    color: Colors.BLACK,
  },
  imageContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 100,
    height: 100,
    marginRight: '5%',
  },
});
