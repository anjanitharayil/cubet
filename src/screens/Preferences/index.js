import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Colors from '../../constants/Colors';
import Header from '../../components/header/Header1';

function Login() {
  return (
    <View style={{flex: 1}}>
      <Header title="Login" />
      <ScrollView contentContainerStyle={styles.container}>
        <View></View>
      </ScrollView>
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.BGCOLOR,
    flex: 1,
    flexGrow: 1,
  },
});
