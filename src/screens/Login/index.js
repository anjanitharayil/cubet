import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Colors from '../../constants/Colors';
import Header from '../../components/header/Header1';
import ScaleDimen from '../../constants/ScaleDimen';
import {CustomInput} from '../../components/input/CustomInput';
import {CustomButton} from '../../components/button/CustomButton';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {setSignIn} from '../../redux/slices/authSlice';

function Login() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  return (
    <View style={styles.main}>
      <Header title="Login" />
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.text}>Welcome Back!</Text>
        <View style={styles.inputContainer}>
          <CustomInput label="Email" />
          <CustomInput label="Password" />
          <Text style={styles.forgot}>Forgot password?</Text>
          <CustomButton
            disabled={false}
            onPress={() => dispatch(setSignIn())}
            title={'Log In'}
          />
          <View style={styles.textContainer}>
            <Text style={[styles.text2, {color: Colors.BLACK}]}>
              Don’t have an account?{' '}
            </Text>
            <Text style={[styles.text2, {color: Colors.PRIMARY_COLOR}]}>
              Register
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  main: {height: '100%', backgroundColor: Colors.WHITE},

  container: {
    flex: 1,
    flexGrow: 1,
    paddingTop: '5%',
  },
  text: {
    fontSize: ScaleDimen.ms32,
    fontWeight: 500,
    color: Colors.PRIMARY_COLOR,
    paddingHorizontal: '5%',
  },
  inputContainer: {
    width: '90%',
    alignSelf: 'center',
    paddingVertical: '5%',
  },
  forgot: {
    fontWeight: 500,
    fontSize: ScaleDimen.ms14,
    color: Colors.PRIMARY_COLOR,
    alignSelf: 'flex-end',
    paddingBottom: '5%',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: '2%',
  },
  text2: {
    fontWeight: 500,
    fontSize: ScaleDimen.ms14,
  },
});
